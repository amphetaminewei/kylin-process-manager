/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STATUSMANAGERINTERFACE_H
#define STATUSMANAGERINTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

/*
 * Proxy class for interface com.kylin.statusmanager.interface
 */
class StatusManagerInterface: public QDBusAbstractInterface
{
    Q_OBJECT

    Q_SLOT void __modeChanged__(bool tablet_mode)
    { Q_EMIT mode_change_signal(tablet_mode); }

public:
    static inline const char *staticInterfaceName()
    { return "com.kylin.statusmanager.interface"; }

public:
    StatusManagerInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = nullptr);

    ~StatusManagerInterface();

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<bool> get_current_tabletmode()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("get_current_tabletmode"), argumentList);
    }

Q_SIGNALS: // SIGNALS
    void mode_change_signal(bool tablet_mode);
};

namespace com {
  namespace kylin {
    namespace statusmanager {
      typedef ::StatusManagerInterface interface;
    }
  }
}
#endif
