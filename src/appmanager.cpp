/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "appmanager.h"
#include <QDebug>
#include <QDBusConnection>
#include "dbusservices/appmanagerservice.h"
#include "dbusservices/applauncher.h"
#include "dbusservices/whitelistmanager.h"
#include "dbusservices/applauncher.h"
#include "core/eventwatcher.h"

AppManager::AppManager(QObject *parent)
    : QObject(parent)
    , m_service(new AppManagerService(this))
    , m_appLauncher(new AppLauncher(this))
    , m_eventWatcher(new EventWatcher(this))
{
    QDBusConnection connection = QDBusConnection::sessionBus();
    if (!connection.registerService("com.kylin.AppManager") ||
            !connection.registerObject("/com/kylin/AppManager", this)) {
        qWarning() << "register dbus service failed" << connection.lastError();
    }
    initConnections();
}

bool AppManager::LaunchApp(const QString &desktopFile)
{
    return m_appLauncher->LaunchApp(desktopFile);
}

bool AppManager::LaunchAppWithArguments(const QString &desktopFile,const QStringList &args)
{
    return m_appLauncher->LaunchAppWithArguments(desktopFile, args);
}

bool AppManager::Open(const QString &fileName)
{
    return m_appLauncher->Open(fileName);
}

bool AppManager::LaunchDefaultAppWithUrl(const QString &url)
{
    return m_appLauncher->LaunchDefaultAppWithUrl(url);
}

void AppManager::SetPowerSavingModeEnable(bool enable)
{
//    m_appLauncher->setPowerSavingModeEnable(enable);
}

bool AppManager::AddToWhiteList(const QString &desktopFile, const QString &option)
{
    return m_whiteListManager.addToWhiteList(desktopFile, option);
}

bool AppManager::RemoveFromWhiteList(const QString &desktopFile, const QString &option)
{
    return m_whiteListManager.removeFromWhiteList(desktopFile, option);
}

bool AppManager::ActiveProcessByWid(const uint &wid)
{
    return m_appLauncher->ActiveProcessByWid(wid);
}

bool AppManager::ActiveProcessByPid(const int &pid)
{
    return m_appLauncher->ActiveProcessByPid(pid);
}

QStringList AppManager::WhiteListsOfApp(const QString &desktopFile)
{
    return m_whiteListManager.whiteListsOfApp(desktopFile);
}

QStringList AppManager::AppWhiteList(const QString &option)
{
    return m_whiteListManager.appWhiteList(option);
}

QVector<QStringList> AppManager::RecommendAppLists(const QString &fileName)
{
    return m_appLauncher->RecommendAppLists(fileName);
}

QString AppManager::AppDesktopFileNameByPid(qint64 pid)
{
    return m_appLauncher->AppDesktopFileNameByPid(pid);
}

QString AppManager::AppDesktopFileNameByWid(qint64 wid)
{
    return m_appLauncher->AppDesktopFileNameByWid(wid);
}

QString AppManager::Inhibit(const QString &desktopFile,
                            uint pid,
                            const QString &reason,
                            uint flags)
{
    return m_appInfoManager.inhibit(desktopFile, pid, reason, flags);
}

void AppManager::UnInhibit(const QString &cookie)
{
    m_appInfoManager.unInhibit(cookie);
}

bool AppManager::ThawApps()
{
    return m_appInfoManager.ThawApps();
}

void AppManager::initConnections()
{
    connect(&m_appInfoManager, &AppInfoManager::appLaunched,
            m_service, &AppManagerService::appLaunched);

    connect(&m_appInfoManager, &AppInfoManager::appFinished,
            m_service, &AppManagerService::appFinished);

    connect(m_eventWatcher, &EventWatcher::activeWindowChanged,
            &m_appInfoManager, &AppInfoManager::onActiveWindowChanged);

    connect(m_eventWatcher, &EventWatcher::windowAdded,
            &m_appInfoManager, &AppInfoManager::onWindowAdded);

    connect(m_eventWatcher, &EventWatcher::windowRemoved,
            &m_appInfoManager, &AppInfoManager::onWindowRemoved);

    connect(m_eventWatcher, &EventWatcher::mprisDbusAdded,
            &m_appInfoManager, &AppInfoManager::onMprisDbusAdded);

    connect(m_eventWatcher, &EventWatcher::tabletModeChanged,
            &m_appInfoManager, &AppInfoManager::onTabletModeChanged);

    connect(m_eventWatcher, &EventWatcher::resourceThresholdWarning,
            &m_appInfoManager, &AppInfoManager::onResourceThresholdWarning);

    connect(m_eventWatcher, &EventWatcher::aboutToQuitApp,
            &m_appInfoManager, &AppInfoManager::onAboutToQuitApp);

    connect(m_eventWatcher, &EventWatcher::ChildPidFinished,
            &m_appInfoManager, &AppInfoManager::onChildPidFinished);
}
