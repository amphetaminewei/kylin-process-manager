/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WHITELISTMANAGER_H
#define WHITELISTMANAGER_H

#include <QObject>
#include <QJsonObject>
#include <QFileSystemWatcher>

class WhiteListManager : public QObject
{
    Q_OBJECT
public:
    explicit WhiteListManager(QObject *parent = nullptr);

    bool addToWhiteList(const QString &desktopFile, const QString &option);
    bool removeFromWhiteList(const QString &desktopFile, const QString &option);
    bool isExists(const QString &desktopFile, const QString &option);
    // 根据应用名称返回白名单列表
    QStringList whiteListsOfApp(const QString &desktopFile);
    // 根据白名单名称返回应用列表
    QStringList appWhiteList(const QString &option);

Q_SIGNALS:
    void addedToWhitelist(QString desktopName);

private:
    void initJsonFile();
    void initConnections();
    void updateWhitelistJsonInfo(const QString &path);

private:
    const QString m_jsonFile;
    QJsonObject m_jsonObj;
    QJsonObject m_jsonObjTest;
    QStringList m_desktopList;
    QFileSystemWatcher *m_whitelistFileWatcher;
    const QStringList appManagerList = {
        "eye-protection-center.desktop",
        "mdm-acpanel.desktop",
        "sogouImeService.desktop",
        "ukui-search-menu.desktop",
        "mate-terminal.desktop",
        "kylin-weather.desktop",
        "ukui-clock.desktop",
        "peony-computer.desktop",
        "peony.desktop",
        "peony-home.desktop",
        "peony-trash.desktop",
        "kylin-connectivity.desktop",
        "ukui-control-center.desktop",
        "startqax.desktop",
        "onboard.desktop",
        "ukui-sidebar.desktop",
        "ukui-panel.desktop",
        "firefox.desktop",
        "google-chrome.desktop",
        "browser360-cn.desktop",
        "qaxbrowser-safe.desktop"

    };
    const QStringList appUninstallList = {
        "kylin-screenshot.desktop",
        "ukui-notebook.desktop",
        "ukui-clock.desktop",
        "kylin-calculator.desktop",
        "kylin-recorder.desktop",
        "kylin-software-center.desktop",
        "kylin-camera.desktop",
        "biometric-manager.desktop",
        "yhkylin-backup-tools.desktop",
        "box-manager.desktop",
        "ukui-system-monitor.desktop",
        "ksc-defender.desktop",
        "logview.desktop",
        "kylin-service-support.desktop",
        "kylin-user-guide.desktop",
        "ukui-control-center.desktop",
        "peony.desktop",
        "engrampa.desktop"
    };

};

#endif // WHITELISTMANAGER_H
