/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EVENTWATCHER_H
#define EVENTWATCHER_H

/*
 * 负责外部事件信号的监听，经过处理后转发给内部模块
 * wayland环境下监听kysdk的窗管信号
*/
#include <QObject>
#include <KWindowSystem>
#include <windowmanager/windowmanager.h>
#include "dbusinterfaces/statusmanagerinterface.h"
#include "dbusinterfaces/kwininterface.h"
#include "policy.h"
#include "processmanager_interface.h"
#include "dbusservices/applauncherdaemon.h"

class EventWatcher : public QObject
{
    Q_OBJECT
public:
    explicit EventWatcher(QObject *parent = nullptr);

    static bool isTabletMode();

Q_SIGNALS:
    void tabletModeChanged(bool isTablet);
    void activeWindowChanged(quint32 currentWid, int currentPid, quint32 preWid, int prePid);
    void windowAdded(quint32 wid, int pid);
    void windowRemoved(quint32 wid);
    void mprisDbusAdded(int pid, const QString &serviceName);
    void resourceThresholdWarning(Policy::Feature resource, Policy::ResourceUrgency level);
    void aboutToQuitApp();
    void ChildPidFinished(const int &pid);

private Q_SLOTS:
    void onTabletModeChanged(bool tabletMode);
    void onActiveWindowChanged(quint32 wid);
    void onWindowAdded(quint32 wid);
    void onWindowRemoved(quint32 wid);
    void onActiveWaylandWindowChanged(kdk::WindowId wid);
    void onWaylandWindowAdded(kdk::WindowId wid);
    void onWaylandWindowRemoved(kdk::WindowId wid);
    void onDbusNameAcquired(QString name, QString newOwner, QString oldOwner);
    void onResourceThresholdWarning(const QString &resource, int level);
    void onAboutToQuitApp();
    void onChildPidFinished(const int &pid);

private:
    /**
     * @brief initConnections
     */
    void initConnections();

    void doWindowActiveChanged(quint32 wid);

private:
    static bool m_isTablet;
    const QString mprisPrefix = "org.mpris.MediaPlayer2.";
    com::kylin::statusmanager::interface *m_statusManagerInterface;
    org::ukui::KWin *m_kwinInterface;
    com::kylin::ProcessManager *m_processManager;
    AppLauncherDaemon *m_appLauncherDaemon;
};

#endif // EVENTWATCHER_H
