/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QDBusConnection>
#include <QDebug>
#include <QDBusMetaType>
#include <QtSingleApplication>
#include "src/appmanager.h"
#ifndef QT_DEBUG
#include "ukui-log4qt.h"
#endif

int main(int argc, char *argv[])
{
    QtSingleApplication a("kylin-app-manager",argc, argv);
    qDBusRegisterMetaType<QVector<QStringList>>();
    if (a.isRunning()) {
        return EXIT_SUCCESS;
    }

#ifndef QT_DEBUG
     initUkuiLog4qt("kylin-app-manager");
#endif

    AppManager appManager;

    return a.exec();
}
