/*
 * Copyright (C) 2020 The Qt Company Ltd.
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KWININTERFACE_H
#define KWININTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

/*
 * Proxy class for interface org.ukui.KWin
 */
class KWinInterface: public QDBusAbstractInterface
{
    Q_OBJECT

    Q_SLOT void __activeWindowChanged__()
    { Q_EMIT activeWindowChanged(); }

public:
    static inline const char *staticInterfaceName()
    {
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
    return "org.ukui.KWin";
#else
        return "org.kde.KWin";
#endif
    }

public:
    KWinInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = nullptr);

    ~KWinInterface();

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<uint> getActiveWindowId()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("getActiveWindowId"), argumentList);
    }

    inline QDBusPendingReply<uint> getWindowPid(uint wid)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(wid);
        return asyncCallWithArgumentList(QStringLiteral("getWindowPid"), argumentList);
    }

    inline QDBusPendingReply<uint> getWidFromPid(uint pid)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(pid);
        return asyncCallWithArgumentList(QStringLiteral("getWidFromPid"), argumentList);
    }

    inline QDBusPendingReply<bool> activeWindowByPid(uint pid)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(pid);
        return asyncCallWithArgumentList(QStringLiteral("activeWindowByPid"), argumentList);
    }

Q_SIGNALS: // SIGNALS
    void activeWindowChanged();
};

namespace org {
  namespace ukui {
    typedef ::KWinInterface KWin;
  }
}
#endif
