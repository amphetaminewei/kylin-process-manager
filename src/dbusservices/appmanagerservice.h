/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef APPMANAGERSERVICE_H
#define APPMANAGERSERVICE_H

#include <QtCore/QObject>
#include <QtDBus/QtDBus>
QT_BEGIN_NAMESPACE
class QByteArray;
template<class T> class QList;
template<class Key, class Value> class QMap;
class QString;
class QStringList;
class QVariant;
QT_END_NAMESPACE

/*
 * Adaptor class for interface com.kylin.AppManager
 */
class AppManagerService: public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.kylin.AppManager")
    Q_CLASSINFO("D-Bus Introspection", ""
"  <interface name=\"com.kylin.AppManager\">\n"
"    <method name=\"LaunchApp\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"desktopFile\"/>\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <method name=\"ActiveProcessByWid\">\n"
"      <arg direction=\"in\" type=\"u\" name=\"wid\"/>\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <method name=\"ActiveProcessByPid\">\n"
"      <arg direction=\"in\" type=\"i\" name=\"pid\"/>\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <method name=\"LaunchAppWithArguments\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"desktopFile\"/>\n"
"      <arg direction=\"in\" type=\"as\" name=\"args\"/>\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <method name=\"LaunchDefaultAppWithUrl\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"url\"/>\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <method name=\"AppDesktopFileNameByPid\">\n"
"      <arg direction=\"in\" type=\"x\" name=\"pid\"/>\n"
"      <arg direction=\"out\" type=\"s\" name=\"desktopFileName\"/>\n"
"    </method>\n"
"    <method name=\"AppDesktopFileNameByWid\">\n"
"      <arg direction=\"in\" type=\"x\" name=\"wid\"/>\n"
"      <arg direction=\"out\" type=\"s\" name=\"desktopFileName\"/>\n"
"    </method>\n"
"    <method name=\"SetPowerSavingModeEnable\">\n"
"      <arg direction=\"in\" type=\"b\" name=\"enable\"/>\n"
"    </method>\n"
"    <method name=\"AddToWhiteList\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"desktopFile\"/>\n"
"      <arg direction=\"in\" type=\"s\" name=\"option\"/>\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <method name=\"RemoveFromWhiteList\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"desktopFile\"/>\n"
"      <arg direction=\"in\" type=\"s\" name=\"option\"/>\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <method name=\"WhiteListsOfApp\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"desktopFile\"/>\n"
"      <arg direction=\"out\" type=\"as\" name=\"whiteLists\"/>\n"
"    </method>\n"
"    <method name=\"AppWhiteList\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"option\"/>\n"
"      <arg direction=\"out\" type=\"as\" name=\"appLists\"/>\n"
"    </method>\n"
"    <method name=\"RecommendAppLists\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"fileName\"/>\n"
"      <arg direction=\"out\" type=\"aas\" name=\"appLists\"/>\n"
"      <annotation value=\"QVector &lt;QStringList&gt;\" name=\"org.qtproject.QtDBus.QtTypeName.Out0\"/>\n"
"    </method>\n"
"    <method name=\"Open\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"fileName\"/>\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <method name=\"Inhibit\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"desktopFile\"/>\n"
"      <arg direction=\"in\" type=\"u\" name=\"pid\"/>\n"
"      <arg direction=\"in\" type=\"s\" name=\"reason\"/>\n"
"      <arg direction=\"in\" type=\"u\" name=\"flags\"/>\n"
"      <arg direction=\"out\" type=\"s\" name=\"cookie\"/>\n"
"    </method>\n"
"    <method name=\"UnInhibit\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"cookie\"/>\n"
"    </method>\n"
"    <method name=\"ThawApps\">\n"
"      <arg direction=\"out\" type=\"b\" name=\"succeed\"/>\n"
"    </method>\n"
"    <signal name=\"appLaunched\">\n"
"      <arg type=\"s\" name=\"desktopNmae\"/>\n"
"      <arg type=\"x\" name=\"pid\"/>\n"
"      <arg type=\"u\" name=\"wid\"/>\n"
"    </signal>\n"
"    <signal name=\"appFinished\">\n"
"      <arg type=\"s\" name=\"desktopNmae\"/>\n"
"      <arg type=\"x\" name=\"pid\"/>\n"
"      <arg type=\"u\" name=\"wid\"/>\n"
"    </signal>\n"
"  </interface>\n"
        "")
public:
    AppManagerService(QObject *parent);
    virtual ~AppManagerService();

public: // PROPERTIES
public Q_SLOTS: // METHODS
    bool ActiveProcessByPid(int pid);
    bool ActiveProcessByWid(uint wid);
    bool AddToWhiteList(const QString &desktopFile, const QString &option);
    QString AppDesktopFileNameByPid(qlonglong pid);
    QString AppDesktopFileNameByWid(qlonglong wid);
    QStringList AppWhiteList(const QString &option);
    QString Inhibit(const QString &desktopFile, uint pid, const QString &reason, uint flags);
    bool LaunchApp(const QString &desktopFile);
    bool LaunchAppWithArguments(const QString &desktopFile, const QStringList &args);
    bool LaunchDefaultAppWithUrl(const QString &url);
    bool Open(const QString &fileName);
    QVector <QStringList> RecommendAppLists(const QString &fileName);
    bool RemoveFromWhiteList(const QString &desktopFile, const QString &option);
    void SetPowerSavingModeEnable(bool enable);
    bool ThawApps();
    void UnInhibit(const QString &cookie);
    QStringList WhiteListsOfApp(const QString &desktopFile);
Q_SIGNALS: // SIGNALS
    void appFinished(const QString &desktopNmae, qlonglong pid, uint wid);
    void appLaunched(const QString &desktopNmae, qlonglong pid, uint wid);
};

#endif
